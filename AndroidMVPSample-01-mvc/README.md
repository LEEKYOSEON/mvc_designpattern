# Android MVC Sample

[![License](https://img.shields.io/hexpm/l/plug.svg)]()

Android MVC Sample.

## Sample list

- Java : [branch : 01-MVC](https://github.com/taehwandev/AndroidMVPSample/tree/01-mvc/app_java)
- Kotlin : [branch : 01-MVC](https://github.com/taehwandev/AndroidMVPSample/tree/01-mvc/app_kotlin)

## Blog post

한국어 자료
- [Android MVP 무작정 따라하기 - MVC 구조 이해하기](http://thdev.tech/androiddev/2016/10/23/Android-MVC-Architecture.html)

## License

```
Copyright 2016 Tae-hwan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
